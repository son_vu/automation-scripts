require 'page-object'

class CommonDefs
	include PageObject
 
	page_url('https://id.atlassian.com/login') # URL used to login to system. 
	text_field(:email, :id => 'username') # Declare Username text field.
	text_field(:password, :id => 'password') # Declare Password text field.
	button(:login_button, :id => 'login-submit') # Declare Login button.
	
	def loginAs(username, password)
		self.goto # Navigate to the page-url declared above.
		self.email = username # Type Username from user input.
		self.password = password # Type Password from user input.
		self.login_button # Click on the Login button. 
	end
	
	def compareString(expected, actual)
		if (expected == actual)
			return true;
		else
			return false;
		end
	end
end