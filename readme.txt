Below is Pre-requisites and Assumptions for executing successfully test scripts create_issue.rb, edit_issue.rb and search_issue.rb. 

### PRE-REQUISITES 
- Following gems are installed successfully for test box running these test scripts.: 
bigdecimal (1.2.4)
childprocess (0.5.6)
data_magic (0.21)
faker (1.4.3)
ffi (1.9.8 x64-mingw32)
i18n (0.7.0)
io-console (0.4.3)
json (1.8.1)
minitest (4.7.5)
multi_json (1.11.0)
page-object (1.1.0)
page_navigation (0.9)
psych (2.0.5)
rake (10.1.0)
rdoc (4.1.0)
rubyzip (1.1.7)
selenium-webdriver (2.45.0)
test-unit (2.1.6.0)
websocket (1.2.2)
yml_reader (0.5)

- Firefox browser is installed on test box running these test scripts. 

### ASSUMPTIONS 
- A project named "TST" created in JIRA system prior to run these test scripts and it should look like "https://jira.atlassian.com/projects/TST"
- An issue with ID "TST-67391" created prior to run test script "edit_issue.rb" and "search_issue.rb". 

### KNOWN ISSUES
- Reloading page (line 26, edit_issue.rb) occassionally leads to Unexpected error due to model dialog popup when the method called. 