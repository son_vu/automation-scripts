require 'selenium-webdriver'
require_relative './common' # Load common.rb file. 

@issue_id = 'TST-67391'; # The issue ID under search. 

@driver = Selenium::WebDriver.for :firefox;
@driver.navigate.to "https://jira.atlassian.com/projects/TST/summary" # Navigate to Project homepage.
@search_input = @driver.find_element(:id, 'quickSearchInput');
@search_input.send_keys(@issue_id);
@search_input.submit;

### CHECKPOINT: Check if the issue under search exists or not. 
cd = CommonDefs.new(@driver); # Create new instance of CommonDefs class. 
@issue_id_after = @driver.find_element(:id, 'key-val').text; # Get issue ID after search.
@result = cd.compareString(@issue_id, @issue_id_after); # Compare issue id between input and output. 
if (@result == true)
	puts "The issue you're looking for is existing."
else
	puts "The issue you're looking for is NOT existing."
end
###----------------------------------

@driver.quit;

