### Create a New Issue. 
require 'selenium-webdriver'
require_relative './common' # Load login.rb file. 

current_datetime = DateTime.now
current_datetime_formated = current_datetime.strftime "%d%m%Y%H%M%S" # Get current Date and Time.

@username = 'vutson@gmail.com';
@password = '12345678';
@summary = '[issue title]' + current_datetime_formated;
@description = 'this is description for testing'; 
@driver = Selenium::WebDriver.for :firefox;

login = CommonDefs.new(@driver); # Create new instance of CommonDefs class. 
login.loginAs(@username, @password); # Login to system.
 
@driver.navigate.to "https://jira.atlassian.com/projects/TST/summary" # Navigate to Project homepage.
@driver.find_element(:id, 'create_link').click; # Click on Create button.
@driver.find_element(:id, 'issuetype-field').click; # Click on 'Issue Type' drop-down-list.
@driver.find_element(:link_text, 'Bug').click; # Select Bug as issue type.
@driver.find_element(:id, 'summary').send_keys(@summary);
@driver.find_element(:id, 'description').send_keys(@description);
@driver.find_element(:id, 'create-issue-submit').click;

puts "Awesome! Your issue has been created successfully."

@driver.quit;