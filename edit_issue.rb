### Edit an Issue. 
require 'date'
require 'selenium-webdriver'
require_relative './common' # Load login.rb file. 

current_datetime = DateTime.now
current_datetime_formated = current_datetime.strftime "%d%m%Y%H%M%S" # Get current Date and Time.

### Declare necessary variables. 
@username = 'vutson@gmail.com';
@password = '12345678';
@updated_summary = '[issue title]' + current_datetime_formated;
@description = 'this is description for testing'; 
@url = 'https://jira.atlassian.com/browse/TST-67391';
@driver = Selenium::WebDriver.for :firefox;
###-----------------------------

cd = CommonDefs.new(@driver); # Create new instance of CommonDefs class. 
cd.loginAs(@username, @password); # Login to system.
 
@driver.navigate.to(@url) # Navigate to the issue for editing.
@driver.find_element(:id, 'edit-issue').click; # Click on Edit button.

@driver.find_element(:id, 'summary').send_keys(@updated_summary); # Enter Updated Summary into textbox. 
@driver.find_element(:id, 'edit-issue-submit').click; # Click 'Edit' to update the issue.
@driver.navigate().refresh(); # Reload page to get updated Issue Summary. 

@actual_summary = @driver.find_element(:id, 'summary-val').text; # Get Issue Summary after updated.

### CHECKPOINT: Check if the issue is updated or not. 
result = cd.compareString(@updated_summary, @actual_summary)
if (result == true)
	puts "Your issue has been updated successfully!";
else
	puts "Your issue has NOT been updated successfully";
end
###--------------------------------------------------

@driver.quit;
